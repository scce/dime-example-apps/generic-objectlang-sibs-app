# ObjectLang-SIBs example app

This example illustrates how to create a SIB that references an `ObjectLang` model that already is available in DIME.

At generation time, from such an ObjectLang-SIB a Process model is generated and the GenericSIB is replaced by a ProcessSIB that references this generated Process model.

## Setup the plugin project and run the product

Head over to the [generic-objectlang-sibs](https://gitlab.com/scce/dime-example-extensions/generic-objectlang-sibs) extension project and follow the instructions there to setup the workspace project that is needed to add ObjectLang-SIBs support to DIME.

Run the new instance of DIME including the ObjectLang-SIBs (step 'Run the product' in the other documentation) before proceeding with the next step of this documentation.

## Setup the runtime project

Clone this repository to a folder that will be referred to as 'repo location' in the following.

From the context menu of the Project Explorer select 'Import > Existing projects into workspace'.
In the opening dialog, enter your repo location as root directory.
Mark the checkbox of the `GenericObjectLangSIBApp` project and hit 'Finish'.

Have a look at the demo model `dime-models/interactable/Startup.process` that uses the new ObjectLang-SIB, a GenericSIB with label 'CreateUsers' referencing the data type of the exact same name.

Feel free to try out adding other ObjectLang-SIBs on your own by dragging and dropping `.objects` files from the `dime-models/objects/` folder to a Process model and selecting to create a GenericSIB.

To build and deploy the app, hit the 'Purge and Deploy' button in the Deployment View to build and run the app.
As soon as the app has been deployed you can access it by opening http://127.0.0.1:9090 in a browser.